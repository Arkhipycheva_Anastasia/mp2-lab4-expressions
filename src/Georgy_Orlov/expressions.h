#pragma once
#include <iostream>
#include "tstack.h"
#include <string>
using namespace std;

#define MAXBUFSIZE 256

bool IsOperation(const char &symb)
{
	if ((symb == '+') || (symb == '-') || (symb == '*') || (symb == '/'))
		return true;
	return false;
}

int Priority(const char &symb)
{
	switch (symb)
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;
	}
}

int ErrorCheck(const string &str, bool print)
{
	TStack BracketStack(MAXBUFSIZE);
	int BracketArr[MAXBUFSIZE][2];
	int ArrSize = 0;
	int errCounter = 0;
	int i = 0;
	while(str[i])
	{
		if (str[i] == '(')
			BracketStack.Put(i);
		else if (str[i] == ')')
		{
			BracketArr[ArrSize][1] = i;
			if (!BracketStack.IsEmpty())
				BracketArr[ArrSize][0] = (int)BracketStack.Get();
			else
			{
				BracketArr[ArrSize][0] = 0;
				errCounter++;
			}
			ArrSize++;
		}
		i++;
	}
	while (!BracketStack.IsEmpty())
	{
		BracketArr[ArrSize][0] = (int)BracketStack.Get();
		BracketArr[ArrSize][1] = 0;
		ArrSize++;
		errCounter++;
	}
	if (errCounter != 0)
		cout << "Find " << errCounter << " bracket error(s)." << endl;
	else cout << "Bracket errors not found" << endl;
	if (print)
	{
		cout << "| Open bracket\t|" << " Close bracket\t" << endl;
		for (int i = 0; i < ArrSize; i++)
			cout << "|\t" << BracketArr[i][0] << "\t|\t" << BracketArr[i][1] << "\t|" << endl;
	}
	return errCounter;
}

string Transform(const string &str)
{
	if (ErrorCheck(str, false))
	{
		cout << "Can not transform, find bracket error(s)!" << endl;
		string result = "ERROR";
		return result;
	}
	else
	{
		string result;
		TStack operations(MAXBUFSIZE);
		int i = 0;
		while (str[i])
		{
			if (isdigit(str[i]) || (str[i] == '.'))
				result += str[i];
			if (IsOperation(str[i]))
			{
				if (operations.IsEmpty())
					operations.Put(str[i]);
				else if (Priority(str[i]) > Priority((char)operations.GetLast()))
					operations.Put(str[i]);
				else
				{
					while (true)
					{
						result += (char)operations.Get();
						if (Priority(str[i]) > Priority((char)operations.GetLast()) || operations.IsEmpty())
							break;
					}
					operations.Put(str[i]);
				}
				result += ' ';
			}
			if (Priority(str[i]) == 0)
				operations.Put(str[i]);
			if (Priority(str[i]) == 1)
				while (true)
				{
					if (operations.IsEmpty())
						break;
					if (Priority((char)operations.GetLast()) == 0)
					{
						operations.Get();
						break;
					}
					result += (char)operations.Get();
				}
			i++;
		}
		while (!operations.IsEmpty())
			result += (char)operations.Get();
		return result;
	}
}

double Calculation(const string &str)
{
	string err = "ERROR";
	if (strcmp(Transform(str).c_str(), err.c_str()) == 0)
	{
		cout << "Transform error!" << endl;
		throw 1;
	}
	else
	{
		int i = 0, j = 0;
		TStack calc(MAXBUFSIZE);
		char number[MAXBUFSIZE] = "";
		string Postfix = Transform(str);
		double FirstNum, SecondNum;
		while (Postfix[i])
		{
			if (isdigit(Postfix[i]) || Postfix[i] == '.')
				number[j++] += Postfix[i];
			else
			{
				if (*number != 0)
				{
					calc.Put(atof(number));
					memset(number, 0, MAXBUFSIZE*sizeof(char));
					j = 0;
				}
				if (Postfix[i] == ' ')
				{
					i++;
					continue;
				}
				FirstNum = calc.Get();
				SecondNum = calc.Get();
				switch (Postfix[i])
				{
				case '+': calc.Put(SecondNum + FirstNum); break;
				case '-': calc.Put(SecondNum - FirstNum); break;
				case '*': calc.Put(SecondNum * FirstNum); break;
				case '/': calc.Put(SecondNum / FirstNum); break;
				}
			}
			i++;
		}
		return calc.Get();
	}
}