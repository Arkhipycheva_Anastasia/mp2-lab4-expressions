#include "expressions.h"
#include "tsimplestack.h"
#include <cctype>
#include <cstdio>
#include <exception>

using std::cout;
using std::endl;
using std::exception;


//�������, ������������ ��������� ��������
int GetPriority(const char &c)
{
	switch (c)
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;
	}
}

//������� �����������, �������� �� ������ ���������
bool IsOperation(const char &in)
{
	if ( (in=='+')||(in=='*')||(in=='/')||(in=='-') )
		return true;
	return false;
}

//�������� �� ����� �� � �������, ���� �� ����� �������� ������
void �ollectOnTheStack (TStack2<char,MAX> & oper, string & str)
{
	while(1)
	{
		if ( oper.IsEmpty() )
			break;
		if ( oper.HGet()=='(')
		{
			oper.Get();
			break;
		}
		str+=oper.HGet();
		oper.Get();
	}
}

//�������� ��������� �� ���������� ������������ ������
int ControllingParentheses (const string &ArExp, bool print)
{
//� ���� ����� �������� ������ ������������� ������
	TStack2<unsigned char,MAX> Control;
	int i=0;

	int SizeError=0; //����� ������� ��������� �� ��������� ������������� ������
	int index=0;//���������� ������ ������
	while (ArExp[i])
	{
		if (ArExp[i]=='(')
			Control.Put(++index);
		if (ArExp[i]==')')
//�� ������ ����������� ������
			if (Control.IsEmpty() )
			{
				
				SizeError++;
				if (print)
					cout <<'-'<< ' '<< ++index<<endl;
				else
					++index;
			}
			else
				if (print)
					cout <<(int)Control.Get()<< ' '<< ++index<<endl;
				else
				{
					Control.Get();
					++index;
				}
		i++;
	}

//���� ����� ��������� ���� ��������, � ����� �������� �������� ������
//������ �� ������ ������������� ������
	if ( !Control.IsEmpty() )
	{
		SizeError++;
		if (print)
			cout <<(int)Control.Get()<< ' '<< '-'<<endl;
		else
			Control.Get();
	}

	if (print)
		if (SizeError==0)
				cout <<"no errors"<<endl;
		else
			cout <<"mismatch brackets "<<SizeError<<endl;
	return SizeError;
}

//�������
string Transfer (const string &ArExp)
{
	try
	{
//����������� ����������, ���� ������� ������ ����� �� ���������� ������������ ������
		if ( ControllingParentheses(ArExp,false) ) throw  exception("mismatch brackets");
		string result;//������, ������� ����� �������� �������� �������
		TStack2<char,MAX> oper;//������ ���� ��������, ����� ������� ������� ��������
		int i=0;

		while (ArExp[i])
		{
//���� �����, ����� �������� � �������� ������, ��������� ���� ������ ������������� ����� 
			if ( isdigit(ArExp[i]) || ArExp[i]=='.')
				result+=ArExp[i];
//���� ������ �������� �������� �������� ����� ��������� � �������� ������ �� �����
			if ( IsOperation(ArExp[i]) )
			{
//���� ���� ������, �������� �������� � ����� �����
				if (oper.IsEmpty())
					oper.Put(ArExp[i]);
// ���� ��������� ������� �������� ����, ��� ���������
// ��������� �������� � �����, �������� ���� � ���� ...
				else
					if ( GetPriority(ArExp[i])>GetPriority(oper.HGet() ) )
					oper.Put(ArExp[i]);
// ... ����� ��������� �� ����� ��� ��������, ����
// �� �������� �������� � ����� ������ �����������
					else
					{
						while(1)
						{
							result+=oper.Get();
							if ( GetPriority(ArExp[i])>GetPriority( oper.HGet() ) || oper.IsEmpty() )
								break;
						}
// �� �������� �������� � ���� ������� ��������
						oper.Put(ArExp[i]);
					}
//���� �� �����, ��������� ������, ����� � ���������� �������� �����
				result+=' ';
			}

// ���� ������� ������ ��� ����������� ������, �������� �� � ���� ��������
// ���� ������ ��� ����������� ������, ��������� �� ����� �������� �
// �������� ���� ��� ��������, ���� �� �������� ����������� ������
// ���� ������ ��� ���� ������������
			if (ArExp[i]=='(' )
				oper.Put(ArExp[i]);

			if (ArExp[i]==')' )
				�ollectOnTheStack (oper, result);

			i++;
		}
// ���� �� ��������� ������� ������ � ��������, � ����� ��������
// ��� ����� ��������, ��������� �� � �������
		while (!oper.IsEmpty())
		{
			result+=(oper.HGet());
			oper.Get();
		}
		return result;
	}
//����������� ��� ��� ����������, ����� ������� � ������
	catch(exception exc)
	{
		throw exception();
	}
}

//����������
float Computation (const string &ArExp)
{
	int i=0,j=0;
//��������� ���� ��� ���������, ����� � ��� ����������� ����������
	TStack2<float,MAX> comput; 
//������ ����� ��������� �� ���� � �����, ����������� ���������� �����
	char number [MAX]="";
	float temp;//��������� ��� ���������������
	
//��������� � ����������� ����� 
	string PostFix=Transfer(ArExp);

	while (PostFix[i])
	{
//���� ��� �����, ��������� � ����������� ������� ����� �������� �����
		if ( isdigit(PostFix[i]) || PostFix[i]=='.')
			number[j++]+=PostFix[i];
		else
		{
//...����� �� ������ ��������� �����
			if (*number!=0)
			{
//��������������� ����� �� float � ������� � ���� �������
				comput.Put((float)atof(number));
//����� ������������, ���������� �������� ��� ��������� �����
				memset(number,0,MAX*sizeof(char));
				j=0;
			}
//������-������� �������� ������� ����� �����, ��� � ��������� �������� �����
			if (PostFix[i]==' ')
			{
				i++;
				continue;
			}
//�������� �� ����� ��� ��������� ����� � ���������� ������
			temp=comput.Get();
			switch (PostFix[i])
			{
			case '+': comput.Put( comput.Get()+temp ); break;
			case '-': comput.Put( comput.Get()-temp ); break;
			case '*': comput.Put( comput.Get()*temp ); break;
			case '/': comput.Put( comput.Get()/temp ); break;
			}
		}
		i++;
	}
//��������� � ����� ��������� � ����� � ����� �������� �����������
	return comput.Get();
}
